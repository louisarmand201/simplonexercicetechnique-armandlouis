package com.company;

public class RealEstateEvaluator {

    /**
     * This method takes a list of habitations, and prints the address
     * of the one that are eligible for a grant.
     * The conditions to be eligible : the habitation must cost less than 5000 per year,
     * and be accessible to reduced mobility people
     *
     * @param habitations
     */
    public static void printEligibleHabitations(List<Habitation> habitations) {
        for(Habitation habitation : habitations) {
            if (habitation.getAnnualCost() < 5000 && habitation.isAccesible()) {
                System.out.println(
                        "Habitation located at : " +
                        habitation.getAddress() +
                        " is eligible"
                );
            }
        }
    }
}
